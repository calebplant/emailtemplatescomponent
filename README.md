# Email Template Component

## Overview

Component allowing users to edit existing email templates, preview them, and send them directly from the same place.

## Demo (~2 min)

[Here's a link to a demo video for the component](https://www.youtube.com/watch?v=eSRqZJPHp7A)

## Some Screenshots

### Editing Template With Related Object

![Editing Template With Related Object](media/template_with_related.png)

### Preview Template Without Related Object

![Preview Template Without Related Object](media/preview_without_related.png)

## File Overview

### LWC

* **emailHelperComponent** - Parent component containing the Header and Editor components
* **EmailHelperEditor** - Editor window that allows the user to make changes to templates and preview them
* **EmailHelperHeader** - Header that lets the user search for templates, set the recipient, and finally send the email
* **EmailHelperPreview** - Lets the user preview the template within the Editor
* **EmailHelperPreviewOptions** - Allows user to set related object record if neccessary
* **lookup** - Generalized lookup based on [this component](https://github.com/pozil/sfdc-ui-lookup-lwc)

### Apex

* **EmailHelperController** - Controller for the Email Component. Also handles lookups for recipients/template related objects