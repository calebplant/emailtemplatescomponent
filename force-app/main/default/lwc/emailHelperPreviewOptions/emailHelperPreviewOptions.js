import { api, LightningElement } from 'lwc';
import generalSearch from '@salesforce/apex/EmailHelperController.generalizedSearch'

export default class EmailHelperPreviewOptions extends LightningElement {

    @api relatedObjectName;
    @api selectedRecipient;

    relatedObjectSelected = false;

    get relatedEntitySearchLabel() {
        return 'Related Record (' + this.relatedObjectName + ')';
    }

    // Disable preview button until all neccessary fields have values
    get previewButtonDisabled() {
        if(this.selectedRecipient) {
            if (this.relatedObjectName == undefined || (this.relatedObjectName != undefined && this.relatedObjectSelected)) {
                return false;
            }
        }
            return true;
    }

    // Search for related object
    handleRelatedObjSearch(event) {
        console.log('emailHelperPreviewOptions :: handleRelatedObjSearch');
        const target = event.target;
        let searchTerm = event.detail.searchTerm;
        
        generalSearch({searchTerm: searchTerm, objectApiName: this.relatedObjectName, iconName: 'standard:record_lookup', searchField: ''})
        .then(results => {
            target.setSearchResults(results);
            console.log('successful search:');
            console.log(JSON.parse(JSON.stringify(results)));
        })
        .catch(error => {
            console.log('Error getting related objects:');
            console.log(error);
        });
    }

    handleRelatedObjSelectionChange(event) {
        console.log('Recipient changed!');
        const selectedIds = event.detail;
        console.log(JSON.parse(JSON.stringify(selectedIds)));
        this.relatedObjectSelected = true;
        this.dispatchEvent(new CustomEvent('relatedobjectselect', {detail: selectedIds[0]}));
    }

    handleRenderPreview() {
        console.log('emailHelperPreviewOptions :: handleRenderPreview');
        this.dispatchEvent(new CustomEvent('previewclicked'));
    }
}