import { api, LightningElement } from 'lwc';
import generalSearch from '@salesforce/apex/EmailHelperController.generalizedSearch'
import recipientSearch from '@salesforce/apex/EmailHelperController.recipientSearch';

export default class EmailHelperHeader extends LightningElement {
    
    @api isSendDisabled;
    
    includeSig;

    // Handlers
    // Search for email template
    handleEmailTemplateSearch(event) {
        console.log('emailHelperHeader :: handleEmailTemplateSearch');
        const target = event.target;
        let searchTerm = event.detail.searchTerm;
        
        generalSearch({searchTerm: searchTerm, objectApiName: 'EmailTemplate', iconName: 'standard:email', searchField: 'Name'})
        .then(results => {
            target.setSearchResults(results);
            console.log('successful search:');
            console.log(JSON.parse(JSON.stringify(results)));
        })
        .catch(error => {
            console.log('error searching templates...');
            console.log(error);
        });
    }

    handleEmailTemplateSelectionChange(event) {
        console.log('Selection changed!');
        const selectedIds = event.detail;
        console.log(JSON.parse(JSON.stringify(selectedIds)));
        let templateId = selectedIds ? selectedIds[0] : null;
        this.dispatchEvent(new CustomEvent('templateselect', {detail: templateId}));
    }

    // Search for recipient contact/user/lead
    handleRecipientSearch(event) {
        console.log('emailHelperHeader :: handleRecipientSearch');
        const target = event.target;

        recipientSearch(event.detail)
        .then(results => {
            target.setSearchResults(results);
            console.log('successful search:');
            console.log(JSON.parse(JSON.stringify(results)));
        })
        .catch(error => {
            console.log('Error searching recipients...');
            console.log(error);
        });
    }

    handleRecipientSelectionChange(event) {
        console.log('Selection changed!');
        const selectedIds = event.detail;
        console.log(JSON.parse(JSON.stringify(selectedIds)));
        let recipientId = selectedIds ? selectedIds[0] : null;
        this.dispatchEvent(new CustomEvent('recipientselect', {detail: recipientId}));
    }

    handleIncludeSigChange(event) {
        console.log('emailHelperHeader :: handleIncludeSigChange');
        this.dispatchEvent(new CustomEvent('includesigchange', {detail: event.detail.checked}))
    }

    handleSend() {
        console.log('emailHelperHeader :: handleSend');
        this.dispatchEvent(new CustomEvent('sendclicked'));
    }
}