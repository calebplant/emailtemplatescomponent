import { api, LightningElement } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import saveBodyChanges from '@salesforce/apex/EmailHelperController.saveTemplateBodyChanges';


export default class EmailHelperEditor extends LightningElement {

    // When selected template is changed, get writable copy of its body (updatedBody)
    @api
    get selectedTemplate() {
        return this._selectedTemplate;
    }
    set selectedTemplate(value) {
        this.setAttribute('selectedTemplate', value);
        this._selectedTemplate = value;
        this.updatedBody = value.HtmlValue;
    }
    @api selectedRecipient;
    @api includeSignature;

    updatedBody;
    isSaveModalOpen = false;

    formats = ['font', 'size', 'bold', 'italic', 'underline',
        'strike', 'list', 'indent', 'align', 'link',
        'image', 'clean', 'table', 'header', 'color'];

    // Private
    _selectedTemplate;

    get showEditor() {
        return (this.updatedBody != null);
    }

    @api getUpdatedBody() {
        console.log('emailHelperEditor :: getUpdatedBody');
        return this.updatedBody;
    }

    @api getRelObj() {
        console.log('emailHelperEditor :: getSampleRelObj');
        return this.template.querySelector('c-email-helper-preview').getRelatedObjectId();  
    }

    // Handlers
    handleBodyChange(event) {
        this.updatedBody = event.target.value;
    }

    handleResetBody() {
        console.log('emailHelperEditor :: handleResetBody');
        this.updatedBody= this.selectedTemplate.HtmlValue;
        const editor = this.template.querySelector('lightning-input-rich-text');
        editor.value = this.updatedBody;
    }

    handleShowSaveModal() {
        console.log('emailHelperEditor :: handleSaveChanges');
        this.isSaveModalOpen = true;
    }

    // Attempt to save changes to template body
    handleConfirmSave() {
        console.log('emailHelperEditor :: handleConfirmSave');

        let sampleRelObj = this.template.querySelector('c-email-helper-preview').getRelatedObjectId();  

        console.log('Saving body changes...' + sampleRelObj);

        saveBodyChanges({
            templateId: this.selectedTemplate.Id,
            whoId: this.selectedRecipient,
            whatId: sampleRelObj,
            updatedBody: this.updatedBody
        })
        .then(result => {
            console.log('Successfully updated template body');
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Success',
                    message: 'Template updated',
                    variant: 'success'
                })
            );
        })
        .catch(error => {
            console.log('Error updating template body!');
            console.log(error);
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error updating template',
                    message: error.body.message,
                    variant: 'error'
                })
            );
        });

        this.handleCloseSaveModal();
    }

    handleCloseSaveModal() {
        this.isSaveModalOpen = false;
    }
}