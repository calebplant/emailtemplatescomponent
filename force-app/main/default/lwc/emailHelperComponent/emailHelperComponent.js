import { LightningElement, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import getSelectedTemplate from '@salesforce/apex/EmailHelperController.getSelectedTemplate';
import sendEmail from '@salesforce/apex/EmailHelperController.sendEmail';

export default class EmailHelperComponent extends LightningElement {

    selectedTemplateId;
    selectedTemplate;
    selectedRecipientId;
    includeSignature = false;
    isSendModalOpen = false;

    @wire (getSelectedTemplate, {templateId: '$selectedTemplateId'})
    getTemplateFields(response) {
        if(response.data) {
            console.log('Successfully got fields:');
            // console.log(JSON.parse(JSON.stringify(response.data)));
            console.log(response.data);
            this.selectedTemplate = response.data.hasOwnProperty('Id') ? response.data : undefined;
            // this.selectedTemplate = response.data;
        }
        if(response.error) {
            console.log('Error getting fields:');
            console.log(response.error);
        }
    }

    // Disable send button if not all neccessary fields have values
    get isSendDisabled() {
        if(this.selectedTemplateId != undefined && this.selectedRecipientId != undefined) {
            if(this.selectedTemplate) {
                if(this.selectedTemplate.hasOwnProperty('RelatedEntityType')) {
                    let editor = this.template.querySelector('c-email-helper-editor');
                    if(editor) {
                        return editor.getRelObj() ? false : true;
                    }
                }
            }

            return false;
        }

        return true;
    }

    // Handlers
    handleTemplateSelect(event) {
        console.log('emailHelperComponent :: handleTemplateSelect')

        this.selectedTemplateId = event.detail;
    }

    handleRecipientSelect(event) {
        console.log('emailHelperComponent :: handleRecipientSelect');
        this.selectedRecipientId = event.detail;
    }

    handleIncludeSigChange(event) {
        this.includeSignature = event.detail;   
    }

    handleSendClicked() {
        console.log('emailHelperComponent :: handleSendClicked');
        this.isSendModalOpen = true;
        this.updatedBody = this.template.querySelector('c-email-helper-editor').getUpdatedBody();
    }

    // Attempt to save updates and send email
    handleConfirmSend() {
        console.log('emailHelperComponent :: handleConfirmSend');
        
        let relObj = this.template.querySelector('c-email-helper-editor').getRelObj();

        sendEmail({
            recipientId: this.selectedRecipientId,
            templateId: this.selectedTemplateId,
            relatedObjId: relObj,
            updatedBody: this.updatedBody,
            includeSignature: this.includeSignature,
        })
        .then(result => {
            console.log('Successfully sent email');
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Success',
                    message: 'Email sent',
                    variant: 'success'
                })
            );
        })
        .catch(error => {
            console.log('Error sending email!');
            console.log(error);
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error sending email',
                    message: error.body.message,
                    variant: 'error'
                })
            );
        });

        this.handleCloseSendModal();
    }

    handleCloseSendModal() {
        this.isSendModalOpen = false;
    }
}