import { api, LightningElement } from 'lwc';
import renderPreview from '@salesforce/apex/EmailHelperController.renderPreview';

export default class EmailHelperPreview extends LightningElement {

    @api relatedObjectName;
    @api selectedRecipient;
    @api includeSignature;
    // Get writable copy of template body when selected template is changed (updatedBody)
    @api
    get emailBody() {
        return this._selectedTemplate;
    }
    set emailBody(value) {
        this.setAttribute('emailBody', value);
        this._emailBody = value;
        this.updatedBody = this._emailBody;
    }

    _emailBody;
    updatedBody;

    relatedObjectId;
    renderedBody;
    isPreviewModalOpen = false;

    @api getRelatedObjectId() {
        console.log('emailHelperPreview :: getRelatedObjectId');
        return this.relatedObjectId;
    }

    // Handlers
    handleRelatedObjectSelected(event) {
        console.log('emailHelperPreview :: handleRelatedObjectSelected');
        this.relatedObjectId = event.detail;
        console.log(this.relatedObjectId);
    }

    // Attempt to render template body for preview
    handleRenderPreview() {
        renderPreview({whoId: this.selectedRecipient, whatId: this.relatedObjectId, body: this.updatedBody, includeSignature: this.includeSignature})
        .then(result => {
            console.log('successfully rendered: ');
            console.log(result);
            this.renderedBody = result;
            this.isPreviewModalOpen = true;
        })
        .catch(error => {
            console.log('Error rendering preview:');
            console.log(error);
            this.renderedBody = undefined;
            this.renderError = error.body.message;
            this.isPreviewModalOpen = true;
        })
    }

    handlePreviewClose() {
        console.log('emailHelperPreview :: handlePreviewClose');
        this.isPreviewModalOpen = false;
        this.renderError = undefined;
        this.renderedBody = undefined;
    }
}