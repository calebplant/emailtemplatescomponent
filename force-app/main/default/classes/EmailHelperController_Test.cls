@isTest
public class EmailHelperController_Test {

    private static String TEMPLATE_ONE_DEV_NAME = 'one81297124';
    private static String TEMPLATE_ONE_BODY = 'onebody';
    private static String EXISTING_TEMPLATE_NAME = 'Tester';
    private static String ACCOUNT_ONE_NAME = 'AccOne';
    private static String CON_ONE_NAME = 'dnwawpandiwa';
    
    /*
    @TestSetup
    static void makeData(){
        List<EmailTemplate> temps = new List<EmailTemplate>();
        EmailTemplate temp1 = new EmailTemplate();
        temp1.isActive = true;
        temp1.Name = 'one';
        temp1.DeveloperName = TEMPLATE_ONE_DEV_NAME;
        temp1.TemplateType = 'text';
        temp1.FolderId = UserInfo.getUserId();
        // temp1.RelatedEntityType = 'Account';
        temps.add(temp1);
        insert temps;

        List<Account> accs = new List<Account>();
        Account acc1 = new Account(Name=ACCOUNT_ONE_NAME);
        accs.add(acc1);
        insert accs;
    }
    */
    
    @isTest
    static void doesGetSelectedTemplateReturnProperTemplate()
    {
        createTemplates();
        EmailTemplate temp = [SELECT Id, Name FROM EmailTemplate WHERE DeveloperName = :TEMPLATE_ONE_DEV_NAME];

        Test.startTest();
        EmailTemplate foundTemp = EmailHelperController.getSelectedTemplate(temp.Id);
        Test.stopTest();

        System.assertEquals(temp.Name, foundTemp.Name);
    }

    @isTest(SeeAllData=true)
    static void doesSaveTemplateBodyChangesUpdateBody()
    {
        createAccounts();

        EmailTemplate template = [SELECT Id, HtmlValue FROM EmailTemplate WHERE Name = :EXISTING_TEMPLATE_NAME];
        String updatedBody = 'updatedone';
        Id userId = UserInfo.getUserId();
        Id relatedId = [SELECT Id FROM Account WHERE Name = :ACCOUNT_ONE_NAME].Id;
        String startBody = template.HtmlValue;

        Test.startTest();
        EmailHelperController.saveTemplateBodyChanges(template.Id, userId, relatedId, updatedBody);
        Test.stopTest();

        template = [SELECT Id, HtmlValue FROM EmailTemplate WHERE Name = :EXISTING_TEMPLATE_NAME];
        System.assertNotEquals(startBody, template.HtmlValue);

    }

    @isTest(SeeAllData=true)
    static void doesSendEmailSuccessfullySendAnEmail()
    {
        createAccounts();
        createContacts();

        Id recipientId = [SELECT Id FROM Contact WHERE Name = :CON_ONE_NAME].Id;
        EmailTemplate template = [SELECT Id FROM EmailTemplate WHERE Name = :EXISTING_TEMPLATE_NAME];
        Id relatedId = [SELECT Id FROM Account WHERE Name = :ACCOUNT_ONE_NAME].Id;
        String updatedBody = 'abcde';
        Boolean includeSignature = false;

        Test.startTest();
        EmailHelperController.sendEmail(recipientId, template.Id, relatedId, updatedBody, includeSignature);
        Test.stopTest();

        Integer numOfEmails = Limits.getEmailInvocations();
        // System.assertEquals(1, numOfEmails);
    }

    @isTest
    static void recipientSearchShouldReturnCons()
    {
        createContacts();

        List<Id> fixedResults = new List<Id>(1);
        Id conId = [SELECT Id FROM Contact WHERE Name = :CON_ONE_NAME].Id;
        fixedResults.add(conId);

        Test.setFixedSearchResults(fixedResults);
        List<String> selectedIds = new List<String>();
        String searchTerm = 'awpa';

        List<LookupSearchResult> results = EmailHelperController.recipientSearch(searchTerm, selectedIds);

        System.assertEquals(1, results.size());
        System.assertEquals(conId, results.get(0).getId());
    }

    @isTest(SeeAllData=true)
    static void doesGeneralizedSearchReturnProperResult()
    {
        String searchTerm = EXISTING_TEMPLATE_NAME;
        String objectApiName = 'EmailTemplate';
        String iconName = 'standard:account';
        String searchField = null;

        Test.startTest();
        List<LookupSearchResult> results = EmailHelperController.generalizedSearch(searchTerm, objectApiName, iconName, searchField);
        Test.stopTest();

        System.assertEquals(1, results.size());
    }

    @isTest(SeeAllData=true)
    static void doesRenderPreviewCreatePreview()
    {
        createAccounts();
        createContacts();

        Id recipientId = [SELECT Id FROM Contact WHERE Name = :CON_ONE_NAME].Id;
        EmailTemplate template = [SELECT Id FROM EmailTemplate WHERE Name = :EXISTING_TEMPLATE_NAME];
        Id relatedId = [SELECT Id FROM Account WHERE Name = :ACCOUNT_ONE_NAME].Id;
        String startBody = 'abcde<br>';
        Boolean includeSignature = false;

        Test.startTest();
        String endBody = EmailHelperController.renderPreview(recipientId, relatedId, startBody, includeSignature);
        Test.stopTest();

        User u = [SELECT Id, Signature FROM User WHERE Id = :UserInfo.getUserId()];
        System.assertEquals(startBody, endBody);
    }

    @isTest(SeeAllData=true)
    static void doesGetSampleIdFetchSampleId()
    {
        EmailTemplate template = [SELECT Id FROM EmailTemplate WHERE Name = :EXISTING_TEMPLATE_NAME];

        Test.startTest();
        Id sampleId = EmailHelperController.getSampleId(template.Id);
        Test.stopTest();

        System.assertEquals(Account.sObjectType, sampleId.getSobjectType());
    }
    
    private static void createTemplates()
    {
        List<EmailTemplate> temps = new List<EmailTemplate>();
        EmailTemplate temp1 = new EmailTemplate();
        temp1.isActive = true;
        temp1.Name = 'one';
        temp1.DeveloperName = TEMPLATE_ONE_DEV_NAME;
        temp1.TemplateType = 'text';
        temp1.FolderId = UserInfo.getUserId();
        // temp1.RelatedEntityType = 'Account';
        temps.add(temp1);
        insert temps;
    }

    private static void createAccounts()
    {
        List<Account> accs = new List<Account>();
        Account acc1 = new Account(Name=ACCOUNT_ONE_NAME);
        accs.add(acc1);
        insert accs;
    }

    private static void createContacts()
    {
        List<Contact> cons = new List<Contact>();
        Contact con1 = new Contact(LastName=CON_ONE_NAME, Email='blabla@abc.com');
        cons.adD(con1);
        insert cons;
    }
}
