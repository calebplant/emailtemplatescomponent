public without sharing class EmailHelperController {
    private final static Integer MAX_RESULTS = 5;

    // Simply returns the template with the passed Id
    // Neccessary because EmailTemplate is not supported by UI API in LWC
    @AuraEnabled(Cacheable=true)
    public static EmailTemplate getSelectedTemplate(Id templateId)
    {
        EmailTemplate template = new EmailTemplate();
        if(templateId != null) {
            template = [SELECT Body, Description, DeveloperName, FolderId, FolderName, HtmlValue, Markup, RelatedEntityType, Subject ,Name
                FROM EmailTemplate
                WHERE Id = : templateId
                LIMIT 1];
        }

        return template;
    }

    // Validate and save the updated Email template body
    @AuraEnabled
    public static void saveTemplateBodyChanges(Id templateId, Id whoId, Id whatId, String updatedBody)
    {
        System.debug('START saveTemplateBodyChanges');
        EmailTemplate template = [SELECT Id, RelatedEntityType FROM EmailTemplate
                                WHERE Id = :templateId];

        // Validate body by rendering
        if(whatId == null && template.RelatedEntityType != null) {
            System.debug('whatId was null');
            whatId = getSampleId(templateId);

            if(whatId != null) {
                System.debug('whatId: ' + whatId);
                renderPreview(whoId, whatId, updatedBody, false);
            } else {
                throw new NoRecordException('No record exists with that related object type. Please create a record before creating a template related to the object.');
            }
        }

        // Update body
        EmailTemplate updatedTemplate = [SELECT Id, HtmlValue FROM EmailTemplate
                                        WHERE Id = :templateId];
        updatedTemplate.HtmlValue = updatedBody;
        update updatedTemplate;
    }

    // Renders the passed body, merging fields according to whoId and whatId
    @AuraEnabled(Cacheable=true)
    public static String renderPreview(Id whoId, Id whatId, String body, Boolean includeSignature)
    {
        System.debug('START renderPreview');
        // System.debug('whoId: ' + whoId);
        // System.debug('whatId: ' + whatId);
        // System.debug('body: ' + body);

        List<Messaging.RenderEmailTemplateBodyResult> renderResults = Messaging.renderEmailTemplate(whoId, whatId, new List<String>{body});
        System.debug(renderResults);
        String result = renderResults[0].getMergedBody();

        if(includeSignature) {
            String sig = getUserEmailSignature();
            if(String.isNotBlank(sig)) {                
                result += '<br><br>' + sig;
            }
        }

        return result;
    }

    // Save body updates and send email to recipient
    @AuraEnabled
    public static void sendEmail(Id recipientId, Id templateId, Id relatedObjId, String updatedBody, Boolean includeSignature){
        System.debug('START sendEmail');

        // Update Email
        saveTemplateBodyChanges(templateId, recipientId, relatedObjId, updatedBody);

        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setTargetObjectId(recipientId); 
        email.setWhatId(relatedObjId);
        email.setTemplateId(templateId);
        if(includeSignature == false) {
            email.setUseSignature(false); 
        }
        email.setTreatTargetObjectAsRecipient(true);
        email.setSaveAsActivity(false);

        List<Messaging.SingleEmailMessage> outboundEmails = new List<Messaging.SingleEmailMessage>();
        outboundEmails.add(email);
        System.debug('Sending Email: ' + email);
        Messaging.sendEmail(outboundEmails);
        System.debug('Email sent');
    }

    // Search contacts/users/leads for recipients
    @AuraEnabled(Cacheable=true)
    public static List<LookupSearchResult> recipientSearch(String searchTerm, List<String> selectedIds)
    {
        // Prepare query paramters
        searchTerm += '*';

        // Execute search query
        List<List<SObject>> searchResults = [
            FIND :searchTerm
            IN ALL FIELDS
            RETURNING
                Contact(Id, Name WHERE Id NOT IN :selectedIds),
                Lead(Id, Name WHERE Id NOT IN :selectedIds),
                User(Id, Name WHERE Id NOT IN :selectedIds)
                // EmailTemplate(Id, Name, FolderName WHERE Id NOT IN :selectedIds)
            LIMIT :MAX_RESULTS
        ];

        // Prepare results
        List<LookupSearchResult> results = new List<LookupSearchResult>();

        List<Contact> cons = (List<Contact>) searchResults[0];
        for(Contact eachCon : cons) {
            results.add(
                new LookupSearchResult(
                    eachCon.Id,
                    'Contact',
                    'standard:contact',
                    eachCon.Name,
                    ''
                )
            );
        }

        List<Lead> leads = (List<Lead>) searchResults[1];
        for(Lead eachLead : leads) {
            results.add(
                new LookupSearchResult(
                    eachLead.Id,
                    'Lead',
                    'standard:lead',
                    eachLead.Name,
                    ''
                )
            );
        }

        List<User> users = (List<User>) searchResults[2];
        for(User eachUser : users) {
            results.add(
                new LookupSearchResult(
                    eachUser.Id,
                    'User',
                    'standard:user',
                    eachUser.Name,
                    ''
                )
            );
        }

        // Optionnaly sort all results on title
        results.sort();

        return results;
    }

        
    // Search for results of passed objectApiName type on their designated Name field if no searchField is passed
    @AuraEnabled(Cacheable=true)
    public static List<LookupSearchResult> generalizedSearch(String searchTerm, String objectApiName, String iconName, String searchField)
    {
        if(String.isEmpty(searchField)) {
            searchField = getObjectNameField(objectApiName);
        }
        String nameField = 'Name'; 
        String querySearchTerm = '%' + String.escapeSingleQuotes(searchTerm) + '%';
        String query = 'SELECT Id, ' + nameField + ' FROM ' + objectApiName + ' WHERE ' + searchField + ' LIKE :querySearchTerm';
        System.debug('Query: ' + query);
        System.debug(querySearchTerm);

        List<SObject> searchResults = Database.query(query);

        // Prepare results
        List<LookupSearchResult> results = new List<LookupSearchResult>();
        for(sObject eachObj : searchResults) {
            results.add(new LookupSearchResult(
                eachObj.Id,
                objectApiName + '',
                iconName,
                (String)eachObj.get(nameField),
                ''
            ));
        }

        System.debug(results);
        return results;
    }

    // Get the Name field for the passed object api Name
    private static String getObjectNameField(String objectApiName)
    {
        FieldDefinition f = [SELECT QualifiedApiName FROM FieldDefinition WHERE EntityDefinition.QualifiedApiName = :objectApiName AND IsNameField = TRUE];
        System.debug(f);
        return f.QualifiedApiName;
    }

    // Get the current user's email signature as a string
    private static String getUserEmailSignature()
    {
        User currUser = [Select Id,Signature from User where Id=:userinfo.getuserId()];
        String userSignature = currUser.Signature;
        if (String.isNotBlank(userSignature)) {
            userSignature = userSignature.replace('\n','<br>');
        }
        return userSignature;
    }

    // Get a random Id for a record that can be related to the passed Email Template
    @testVisible
    private static Id getSampleId(Id templateId)
    {
        EmailTemplate template = [SELECT Id, RelatedEntityType FROM EmailTemplate
                                WHERE Id = :templateId];
        if(template.RelatedEntityType != null) {
            String query = 'SELECT Id FROM ' + template.RelatedEntityType;
            List<SObject> objs = Database.query(query);
            if(objs.size() > 0) {
                return (Id)objs[0].get('Id');
            }
        }

        return null;
    }
}
